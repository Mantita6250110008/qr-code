import 'dart:typed_data';
import 'dart:ui';
import 'dart:io';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/rendering.dart';
import 'package:flutter/services.dart';
import 'package:qr_flutter/qr_flutter.dart';
import 'package:path_provider/path_provider.dart';
import 'package:share_plus/share_plus.dart';

class GenQR extends StatefulWidget {
  const GenQR({Key? key}) : super(key: key);

  @override
  _GenQRState createState() => _GenQRState();
}

class _GenQRState extends State<GenQR> {
  final key = GlobalKey();
  String textdata = 'android';
  final textcontroller = TextEditingController();
  File? file;
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      resizeToAvoidBottomInset: false,

      appBar: AppBar(
        title: Text('QR Code Generator With Share Example'),
      ),
      body: Column(
        children: [
          RepaintBoundary(
            key: key,
            child: Container(
              color: Colors.white,
              child: QrImage(
                  size: 300, //size of the QrImage widget.
                  data: textdata, //textdata used to create QR code
                  version: QrVersions.auto,
                  embeddedImage: AssetImage('assets/images/logo1.jpg'),
                  embeddedImageStyle: QrEmbeddedImageStyle(
                      size: Size.square(40),
                      color: Color.fromARGB(100, 10, 10, 10))),
            ),
          ),
          SizedBox(
            height: 50,
          ),
      Padding(
        padding: const EdgeInsets.only(left: 40,right: 40),
        child: TextField(

          decoration:  InputDecoration(border:OutlineInputBorder(
            borderRadius: BorderRadius.circular(25),
            borderSide:BorderSide(
              color: Colors.blue
            )
          )

          ),
              controller: textcontroller,
            ),
      ),


          Padding(
            padding: const EdgeInsets.only(top: 40),
            child: SizedBox(
              width: 300,
              height: 40,
            child:ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              child: Text('Create QR Code'),
              onPressed: () async {
                setState(() {
                  //rebuilds UI with new QR code
                  textdata = textcontroller.text;
                });
              },

            ),
            ),
          ),
          Padding(
            padding: const EdgeInsets.only(top: 40),
            child: SizedBox(
              width: 300,
              height: 40,
            child:ElevatedButton(
              style: ElevatedButton.styleFrom(
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(20))),
              child: Text('Share'),
              onPressed: () async {
                try {
                  RenderRepaintBoundary boundary = key.currentContext!
                      .findRenderObject() as RenderRepaintBoundary;
                  //captures qr image
                  var image = await boundary.toImage();
                  ByteData? byteData =
                      await image.toByteData(format: ImageByteFormat.png);
                  Uint8List pngBytes = byteData!.buffer.asUint8List();
                  //app directory for storing images.
                  final appDir = await getApplicationDocumentsDirectory();
                  //current time
                  var datetime = DateTime.now();
                  //qr image file creation
                  file = await File('${appDir.path}/$datetime.png').create();
                  //appending data
                  await file?.writeAsBytes(pngBytes);
                  //Shares QR image
                  await Share.shareFiles(
                    [file!.path],
                    mimeTypes: ["image/png"],
                    text: "Share the QR Code",
                  );
                } catch (e) {
                  print(e.toString());
                }
              },
            )
            ),
          )

        ],
      ),
    );
  }
}
